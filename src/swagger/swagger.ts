import {servers} from "./swagger.server";
import {
    CreateFolderRequestBody,
    ListFolderFilesRequestBody, UploadMediaFileByBase64RequestBody,
    UploadMediaFileByUrlRequestBody
} from "../storage/swagger/storage.components";
import {storagePaths} from "../storage/swagger/storage.paths";

export const swaggerDefinition = {
    openapi: '3.0.0',
    info: {
        title: 'Consus Storage API',
        description: 'Storage API that persist information in Google Drive',
        version: '1.0.0'
    },
    servers,
    paths: {
        ...storagePaths,
    },
    components: {
        requestBodies: {
            CreateFolderRequestBody,
            UploadMediaFileByUrlRequestBody,
            UploadMediaFileByBase64RequestBody,
            ListFolderFilesRequestBody,
        }
    }
}
