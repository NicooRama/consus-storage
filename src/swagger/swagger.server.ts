export const servers = [
    {
        url: 'http://localhost:{port}',
        description: 'The Local API server',
        variables: {
            port: {
                default: '8050'
            }
        }
    },
    {
        url: 'http://nicolasrama.com:{port}',
        description: 'The Production API server',
        variables: {
            port: {
                default: '8050'
            }
        }
    }
]
