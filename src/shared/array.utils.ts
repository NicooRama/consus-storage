export const isSame = (array1: any[], array2: any[], compareFn?: (element1: any, element2: any) => boolean): boolean => {
    if(!array1 || !array2) return false;
    if(array1.length !== array2.length) return false;

    if(!compareFn) {
        compareFn = (element1: any, element2: any) => element1 === element2;
    }

    for(let i = 0; i<array1.length; i++) {
        if(!compareFn(array1[i], array2[i])){
            return false;
        }
    }

    return true;
}
