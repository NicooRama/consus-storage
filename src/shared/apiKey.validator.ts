import {Request, Response, NextFunction} from 'express';
import {config} from "../core/config";
import {StorageError} from "../core/storage.error";

export const apiKeyValidator = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    if(req.headers['x-api-key'] === config.apiKey) {
        return next();
    }
    return next(new StorageError(401, 'x-api-key not provided or invalid', "Unauthorized"));
}
