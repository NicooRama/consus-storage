import fs from "fs";

export const writeBufferToFile = (name, buffer): Promise<void> => {
    return new Promise((resolve, reject) => {
        fs.writeFile(tempPath(name), buffer,  "binary",function(err) {
            if(err){
                reject(err);
                return;
            }
            resolve();
        });
    })
};


export const readFileToStream = (name) => {
    return fs.createReadStream(tempPath(name));
}

export const readFile = name => {
    return fs.readFileSync(tempPath(name));
}
const tempPath = (fileName: string) => {
    return `/tmp/${fileName}`
}

export const normalizePath = (path?: string): string => {
    if(!path) return "";
    return path.split("/").filter((part => !!part)).join('/')
};

export const splitInParts = (path: string): string[] => {
    return path.split('/').filter(part => !!part);
};

export const normalizeAndSplitInParts = (path?:string): string[] => {
    return splitInParts(normalizePath(path));
};

