import {isSame} from "../array.utils";

describe('array.utils', () => {
    describe('isSame', () => {
        it('returns false because first array is not defined', () => {
            expect(isSame(undefined, ["2"])).toBe(false);
        });

        it('returns false because second array is not defined', () => {
            expect(isSame( ["1"], undefined)).toBe(false);
        })

        it('returns false because arrays hasn\'t the same length', () => {
            expect(isSame(["1", "2"], ["1"])).toBe(false);
        })

        it('returns false because arrays aren\'t the same', () => {
            expect(isSame(["1", "2"], ["2", "1"])).toBe(false);
        })

        it('returns true because arrays are the same', () => {
            const compareFunc = (el1: any, el2: any) => el1.id === el2.id;
            const array1 = [
                {id: 1, name: "uno"},
                {id: 2, name: "dos"},
                {id: 3, name: "tres"},
            ];
            const array2 = [
                {id: 1, name: "uno"},
                {id: 2, name: "dos"},
                {id: 3, name: "tres"},
            ];

            expect(isSame(array1, array2, compareFunc)).toBe(true);
        })
    })
})
