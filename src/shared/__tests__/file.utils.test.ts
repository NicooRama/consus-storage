import {normalizePath} from "../file.utils";

describe('file.utils', () => {
    describe('normalizePath', () => {
        it('returns "" because path is not defined', () => {
            expect(normalizePath()).toBe("");
        });

        it('returns "foo/bar" because path have an extra "/"', () => {
            expect(normalizePath("foo/bar/")).toBe("foo/bar");
        })

        it('returns same path because the path dosen\'t have errors', () => {
            expect(normalizePath("foo/bar")).toBe("foo/bar");
        })
    })
})
