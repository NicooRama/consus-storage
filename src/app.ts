import express from 'express';
import compression from "compression";
import bodyParser from "body-parser";
import cors from "cors";
import * as swaggerUi from 'swagger-ui-express';
import {config} from "./core/config";
import {logger} from "./core/logger";
import {logId, Result} from "@consus/node-core";
import {createErrorHandlerMiddleware} from "@consus/node-core";
import {createStorageErrorHandler} from "./core/error.middleware";
import {storageRouter} from "./storage/storage.router";
import {apiKeyValidator} from "./shared/apiKey.validator";
import {swaggerDefinition} from "./swagger/swagger";
import {connectToMongo} from "./core/mongo";
import {LocalStorageService} from "./storage/providers/local/local.service";

const baseUrl = '/storage';
const apiBaseUrl = `${baseUrl}/api`;

export const app = express();

app.use(`${baseUrl}/public`, express.static(config.rootPath));

connectToMongo();

app.use(cors({credentials: true, origin: true, exposedHeaders: 'X-Total-Count'}));
app.use(compression());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json({limit: '50mb'}));
app.use(logId);
app.use(apiKeyValidator);


app.use(`${apiBaseUrl}/documentation`, swaggerUi.serve, swaggerUi.setup(swaggerDefinition));
// app.use('/api/storage', storageRouter(new GoogleDriveService()));
app.use(`${apiBaseUrl}/storage`, storageRouter(new LocalStorageService()));

app.get(`${apiBaseUrl}/health`, (_, res) => {
    res.json({status: "UP"})
});

app.use(createErrorHandlerMiddleware(logger));
app.use(createStorageErrorHandler(logger));

export const server = app.listen(config.port, () => {
    logger.info(`Storage API listening`, {
        action: 'SERVER_START',
        result: Result.SUCCESS,
        params: {
            mode: process.env.NODE_ENV || 'development',
            port: config.port,
            rootPath: config.rootPath,
        }
    });
});
