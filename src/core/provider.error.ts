export class ProviderError extends Error {
    encapsulatedError: Error;
    provider: string;

    constructor(provider: string, encapsulatedError: Error) {
        super(encapsulatedError.message);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
        this.provider = provider;
        this.encapsulatedError = encapsulatedError;
    }
}
