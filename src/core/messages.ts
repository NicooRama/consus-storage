export const Messages = {
    USER_WRONG_CREDENTIALS: "El usuario no existe o las credenciales son incorrectas",
    USER_EXISTS: "El mail ingresado ya esta en uso",
    USER_REGISTERED: "Te has registrado exitosamente",
    USER_UNAUTHORIZED: "No tienes permisos para realizar esa acción",

    DEFAULT_ERROR: "Ha ocurrido un error inesperado",

    LOG_IN_REQUIRED: "Debes estar logueado",
};