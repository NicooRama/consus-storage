export class StorageError extends Error {
    status: number;
    friendlyMessage: string;

    constructor(status: number, message: string, friendlyMessage: string) {
        super(message);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
        this.status = status;
        this.friendlyMessage = friendlyMessage;
    }
}