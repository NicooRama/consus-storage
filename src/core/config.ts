import {config as dotEnvConfig} from "dotenv";
import path from "path";

dotEnvConfig();

export const config = {
    port: process.env.PORT,
    host: process.env.HOST,
    apiKey: process.env.API_KEY,
    logsPath: process.env.LOGS_PATH,
    tokenPath: process.env.TOKEN_PATH,
    credentialsPath: process.env.CREDENTIALS_PATH,
    rootFolderName: process.env.ROOT_FOLDER_NAME,
    imgBBApiKey: process.env.IMG_BB_API_KEY,
    imgUrAccessToken: process.env.IMG_UR_ACCESS_TOKEN,
    mongoUri: process.env.MONGO_URI,
    sirvClientId: process.env.SIRV_CLIENT_ID,
    sirvClientSecret: process.env.SIRV_CLIENT_SECRET,
    rootPath: path.join(__dirname, '../../public'),
};
