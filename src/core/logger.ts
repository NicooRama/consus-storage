import {ApiLogger} from "@consus/node-core";
import {config} from "./config";

export const logger: ApiLogger = new ApiLogger(config.logsPath);
