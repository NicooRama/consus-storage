import mongoose from 'mongoose';
import {config} from "./config";
import {logger} from "./logger";
import {Result} from "@consus/node-core";

const START_DB_ACTION = 'START_DB';

export const connectToMongo = () => {
    logger.info('Starting database connection...', {action: START_DB_ACTION, result: Result.IN_PROGRESS});
    mongoose.connect(config.mongoUri, {useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true}, (error) => {
        if (!error) {
            logger.info('Database connection started...', {action: START_DB_ACTION, result: Result.SUCCESS});
        }else {
            logger.error('Error ocurred on mongo connection', {error});
        }
    });
}
