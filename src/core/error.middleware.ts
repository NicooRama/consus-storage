import {NextFunction, Request, Response} from "express";
import {ApiLogger, LoggedRequest} from "@consus/node-core";
import {ApiError} from "@consus/node-core";
import {StorageError} from "./storage.error";
import {Messages} from "./messages";
import {ProviderError} from "./provider.error";

export const createStorageErrorHandler = (logger: ApiLogger) => {
    return (error: Error, req: LoggedRequest, res: Response, next: NextFunction): Response => {
        switch (error.name) {
            case 'ProviderError':
                const providerError: ProviderError = error as ProviderError;
                logger.error(`Error found on provider request - Provider=${providerError.provider}`, {
                    req,
                    error: providerError.encapsulatedError,
                })
                return res.status(500).send(Messages.DEFAULT_ERROR)
            case 'StorageError':
                const storageError: StorageError = error as StorageError;
                logger.error(`Internal error found`, {
                    req,
                    error,
                })
                return res.status(storageError.status).send(storageError.friendlyMessage);
            default:
                logger.error(`Unexpected error`, {
                    req,
                    error,
                })
                return res.status(500).send(Messages.DEFAULT_ERROR);
        }
    }
};
