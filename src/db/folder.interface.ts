export interface Folder {
    _id: string;
    externalId: string;
    name: string;
    provider: string;
}
