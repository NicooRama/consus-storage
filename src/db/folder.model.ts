import {Document, model, Schema} from 'mongoose';
import { Folder } from './folder.interface';

export const FolderSchema: Schema = new Schema(
    {
        externalId: String,
        name: String,
        provider: String,
    }, {timestamps: true}
);

export const FolderModel = model<Folder & Document>('Folder', FolderSchema, 'folders');
