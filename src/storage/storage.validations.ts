import Joi from 'joi';

export const createFolderSchema = Joi.object({
    name: Joi.string().max(100).required(),
    path: Joi.string().allow(''), //validar que tenga formato de path
})

export const listFolderFilesSchema = Joi.object({
    path: Joi.string().allow(''), //validar que tenga formato de path
})

export const uploadFileSchema = Joi.object({
    name: Joi.string().max(100).required(),
    url: Joi.string().uri().required(),
    path: Joi.string().allow(''), //validar que tenga formato de path
})

export const uploadBase64FileSchema = Joi.object({
    name: Joi.string().max(100).required(),
    data: Joi.string().required().custom((value, helper) => {
        const fileRealLength = 3 * (value.length / 4);
        if ((fileRealLength / 1024 / 1024) > 5) {
            return helper.error("file.length")
        } else {
            return true
        }

    }),
    path: Joi.string().allow(''), //validar que tenga formato de path
})
