import axios from "axios";

export const getImageByUrl = async (url: string) => {
    let response;
    try {
        response = await axios.get(url, {responseType: 'arraybuffer'})
    } catch (e) {
        throw e;
    };
    return response.data;
}

export const formUrlEncoded = obj => {
    return Object.keys(obj).map(key => `${key}=${encodeURIComponent(obj[key])}`).join('&');
}

export function getExtensionFromArrayBuffer(arrayBuffer) {
    const uint8arr = new Uint8Array(arrayBuffer)

    const len = 4
    if (uint8arr.length >= len) {
        let signatureArr = new Array(len)
        for (let i = 0; i < len; i++)
            signatureArr[i] = (new Uint8Array(arrayBuffer))[i].toString(16)
        const signature = signatureArr.join('').toUpperCase()

        switch (signature) {
            case '89504E47':
                // return 'image/png'
                return 'png'
            case '47494638':
                // return 'image/gif'
                return 'gif'
            case '25504446':
                // return 'application/pdf'
                return 'pdf'
            case 'FFD8FFDB':
                // return 'image/jpg'
                return 'jpg'
            case 'FFD8FFE0':
                // return 'image/jpeg'
                return 'jpeg'
            case '504B0304':
                // return 'application/zip'
                return 'zip'
            default:
                return null
        }
    }
    return null
}
