import {
    CreateFolderResponse,
    CreateFolderResult,
    ListFilesResponse,
    StorageService,
    UploadMediaFileResponse
} from "./storage.interface";
import {NextFunction, Response} from "express";
import {logger} from "../core/logger";
import {LoggedRequest, Result} from "@consus/node-core";
import {ProviderError} from "../core/provider.error";

export const StorageController = (storageService: StorageService) => {
    const createFolder = async (req: LoggedRequest, res: Response, next: NextFunction): Promise<void> => {
        const {name, path} = req.body;
        logger.info("Trying to create a new folder", {
            req,
            result: Result.IN_PROGRESS,
            params: {name, path}
        })

        let response: CreateFolderResponse;

        try {
            response = await storageService.createFolder(name, path);
        } catch (e) {
            return next(new ProviderError(storageService.keyName, e));
        }

        logger.info(response.result === CreateFolderResult.CREATED ? "Folder created successfully" : "Folder not created because already exists", {
            req,
            result: Result.SUCCESS,
            params: {name, path}
        });

        res.status(response.result === CreateFolderResult.CREATED ? 201 : 200).json(response);
    }

    const uploadFileByBase64 = async (req: LoggedRequest, res: Response, next: NextFunction): Promise<void> => {
        const {name, path, data} = req.body;
        const dataParam = data.substring(0, 10) + '...';
        logger.info("Trying to upload a file by base64", {
            req,
            result: Result.IN_PROGRESS,
            params: {name, path, data: dataParam}
        })

        let result: UploadMediaFileResponse;

        try {
            result = await storageService.uploadMediaByBase64(name, path, data);
        } catch (e) {
            return next(e);
        }

        logger.info("File upload by base64 successfully", {
            req,
            result: Result.SUCCESS,
            params: {name, path, data: dataParam}
        })

        res.status(200).json(result);
    }

    const uploadFileByUrl = async (req: LoggedRequest, res: Response, next: NextFunction): Promise<void> => {
        const {name, path, url} = req.body;
        logger.info("Trying to upload a file by url", {
            req,
            result: Result.IN_PROGRESS,
            params: {name, path, url}
        })

        let result: UploadMediaFileResponse;

        try {
            result = await storageService.uploadMediaByUrl(name, path, url);
        } catch (e) {
            return next(e);
        }

        logger.info("File upload by url successfully", {
            req,
            result: Result.SUCCESS,
            params: {name, path, url}
        })

        res.status(200).json(result);
    }

    const listFolderFiles = async (req: LoggedRequest, res: Response, next: NextFunction): Promise<void> => {
        const {path} = req.body;
        logger.info("Trying to list folder files", {
            req,
            result: Result.IN_PROGRESS,
            params: {path}
        })

        let result: ListFilesResponse;

        try {
            result = await storageService.listFiles(path);
        } catch (e) {
            return next(e);
        }

        logger.info("Folder files listed successfully", {
            req,
            result: Result.SUCCESS,
            params: {path}
        })

        res.status(200).json(result);
    }

    return {
        createFolder,
        uploadFileByUrl,
        uploadFileByBase64,
        listFolderFiles,
    }
};

