const createFolderResponseContent = {
        'application/json': {
            schema: {
                type: 'object',
                properties: {
                    name: {
                        type: 'string',
                        example: 'Alimentary',
                    },
                    key: {
                        type: 'string',
                        example: '1CJbXl5_830HZI7NKjI0tqyoRRT3ayUzG'
                    }
                }
            }
        }
}

export const storagePaths = {
    '/api/storage/folders': {
        post: {
            summary: 'Create folder',
            description: 'Create a folder in the specified path (default: root)',
            tags: ['Storage'],
            parameters: [],
            security: [],
            requestBody: {
                $ref: '#/components/requestBodies/CreateFolderRequestBody'
            },
            responses: {
                200: {
                    description: 'Folder information of an existing folder',
                    content: createFolderResponseContent,
                },
                201: {
                    description: 'Folder information of a new folder',
                    content: createFolderResponseContent,
                },
            }
        }
    },
    '/api/storage/folders/files': {
        post: {
            summary: 'List folders files',
            description: 'List files in the specified path (default: root)',
            tags: ['Storage'],
            parameters: [],
            security: [],
            requestBody: {
                $ref: '#/components/requestBodies/ListFolderFilesRequestBody'
            },
            responses: {
                200: {
                    description: 'Folder files information',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'array',
                                items: {
                                    type: 'object',
                                    properties: {
                                        name: {
                                            type: 'string',
                                            example: 'Example.jpg'
                                        },
                                        key: {
                                            type: 'string',
                                            example: '1rtvIrv6Q0LwunV1jMsAmVW0J5u0Ng_Tw',
                                        },
                                        previewLink: {
                                            type: 'string',
                                            example: 'https://drive.google.com/uc?export=download&id=1rtvIrv6Q0LwunV1jMsAmVW0J5u0Ng_Tw',
                                        },
                                        link: {
                                            type: 'string',
                                            example: 'https://drive.google.com/file/d/19Nd191u9bCtixmhjHg8PtM3JWRROoVVW/view',
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    },
    '/api/storage/files': {
        post: {
            summary: 'Upload a file',
            description: 'Upload a file in the specified path (default: root)',
            tags: ['Storage'],
            parameters: [],
            security: [],
            requestBody: {
                $ref: '#/components/requestBodies/UploadMediaFileByUrlRequestBody'
            },
            responses: {
                200: {
                    description: 'File information',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                properties: {
                                    name: {
                                        type: 'string',
                                        example: 'Example.jpg'
                                    },
                                    key: {
                                        type: 'string',
                                        example: '1rtvIrv6Q0LwunV1jMsAmVW0J5u0Ng_Tw',
                                    },
                                    previewLink: {
                                        type: 'string',
                                        example: 'https://drive.google.com/uc?export=download&id=1rtvIrv6Q0LwunV1jMsAmVW0J5u0Ng_Tw',
                                    },
                                    link: {
                                        type: 'string',
                                        example: 'https://drive.google.com/file/d/19Nd191u9bCtixmhjHg8PtM3JWRROoVVW/view',
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    },
    '/api/storage/files/base64': {
        post: {
            summary: 'Upload a file',
            description: 'Upload a file in the specified path (default: root)',
            tags: ['Storage'],
            parameters: [],
            security: [],
            requestBody: {
                $ref: '#/components/requestBodies/UploadMediaFileByBase64RequestBody'
            },
            responses: {
                200: {
                    description: 'File information',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                properties: {
                                    name: {
                                        type: 'string',
                                        example: 'Example.jpg'
                                    },
                                    key: {
                                        type: 'string',
                                        example: '1rtvIrv6Q0LwunV1jMsAmVW0J5u0Ng_Tw',
                                    },
                                    previewLink: {
                                        type: 'string',
                                        example: 'https://drive.google.com/uc?export=download&id=1rtvIrv6Q0LwunV1jMsAmVW0J5u0Ng_Tw',
                                    },
                                    link: {
                                        type: 'string',
                                        example: 'https://drive.google.com/file/d/19Nd191u9bCtixmhjHg8PtM3JWRROoVVW/view',
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    },
}
