export const UploadMediaFileByUrlRequestBody = {
    required: true,
    content: {
        'application/json': {
            schema: {
                type: 'object',
                properties: {
                    name: {
                        type: 'string',
                        example: 'Example.jpg',
                    },
                    url: {
                        type: 'string',
                        example: 'https://us.123rf.com/450wm/outchill/outchill1712/outchill171201487/91259550-ejemplo-texto-escrito-en-sello-con-textura-vintage-de-goma-redonda-amarilla-.jpg'
                    },
                    path: {
                        type: 'string',
                        example: '',
                    }
                },
                required: ['name', 'url']
            }
        }
    }
}

export const UploadMediaFileByBase64RequestBody = {
    required: true,
    content: {
        'application/json': {
            schema: {
                type: 'object',
                properties: {
                    name: {
                        type: 'string',
                        example: 'Example.jpg',
                    },
                    data: {
                        type: 'string',
                    },
                    path: {
                        type: 'string',
                        example: '',
                    }
                },
                required: ['name', 'url']
            }
        }
    }
}

export const CreateFolderRequestBody = {
    required: true,
    content: {
        'application/json': {
            schema: {
                type: 'object',
                properties: {
                    name: {
                        type: 'string',
                        example: 'Alimentary'
                    },
                    path: {
                        type: 'string',
                        example: "",
                    }
                },
                required: ['name']
            }
        }
    }
}

export const ListFolderFilesRequestBody = {
    required: true,
    content: {
        'application/json': {
            schema: {
                type: 'object',
                properties: {
                    path: {
                        type: 'string',
                        example: "",
                    }
                },
            }
        }
    }
}

