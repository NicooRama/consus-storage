import {Router} from "express";
import {validateBody} from "@consus/node-core";
import {StorageService} from "./storage.interface";
import {
    createFolderSchema,
    listFolderFilesSchema,
    uploadBase64FileSchema,
    uploadFileSchema
} from "./storage.validations";
import {StorageController} from "./storage.controller";

export const storageRouter = (storageService: StorageService): Router => {
    const router = Router();
    const controller = StorageController(storageService);

    router.post('/folders', [validateBody(createFolderSchema)], controller.createFolder)
    router.post('/folders/files', [validateBody(listFolderFilesSchema)], controller.listFolderFiles)
    router.post('/files', [validateBody(uploadFileSchema)], controller.uploadFileByUrl)
    router.post('/files/base64', [validateBody(uploadBase64FileSchema)], controller.uploadFileByBase64)

    return router;
}
