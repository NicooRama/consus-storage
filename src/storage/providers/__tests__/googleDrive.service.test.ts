import {StorageService} from "../../storage.interface";

jest.setTimeout(60000000);

jest.mock('googleapis', () => ({
    google: {
        auth: {
            OAuth2: function () {
                this.setCredentials = function () {};
            },
        },
        drive: ({version, auth}: {version:string, auth: any}) => {
            return ({
                files: {
                    create: jest.fn(),
                    list: jest.fn(),
                    get: jest.fn(),
                }
            })
        }
    }
} as any));

jest.mock('axios', () => ({
    get: jest.fn(),
}))

import {GoogleDriveService} from "../googleDrive/googleDrive.service";
import axios from 'axios';

describe('googleDrive.service', () => {
    const googleDriveService = new GoogleDriveService();
    const service: StorageService = googleDriveService;

    describe('createFolder', () => {
        beforeEach(() => {
            googleDriveService.drive.files.list.mockClear();
            googleDriveService.drive.files.create.mockClear();
        })

        it('Create a folder that not exists', async () => {
            googleDriveService.drive.files.list.mockImplementation((name: string) => ({data: {files: [{id: '1', name: 'Storage'}]}}));
            googleDriveService.drive.files.create.mockImplementationOnce(({resource}) => ({data: {id: '2', name: resource.name}}));

            const name = 'Alimentary';
            const folder = await service.createFolder(name);
            expect(googleDriveService.drive.files.list).toBeCalledWith({"fields": "files(id, name, parents)", "q": `name='${googleDriveService.ROOT_FOLDER_NAME}' or name='${name}' and mimeType = 'application/vnd.google-apps.folder' and trashed = false`})
            expect(googleDriveService.drive.files.list).toBeCalledWith({"fields": "files(id, name, parents)", "q": `name='${googleDriveService.ROOT_FOLDER_NAME}' and mimeType = 'application/vnd.google-apps.folder' and trashed = false`});
            expect(folder).toHaveProperty('key', '2')
            expect(folder).toHaveProperty("name", name);

        });

        it('Doesn\'t create the folder because that folder already exists', async () => {
            const name = 'Alimentary';
            googleDriveService.drive.files.list.mockImplementation((opts: any) => ({data: {files: [{id: '1', name: 'Storage'}, {id: '2', name, parents: ['1'] }]}}));
            const folder = await service.createFolder(name);
            expect(googleDriveService.drive.files.list).toBeCalledWith({"fields": "files(id, name, parents)", "q": `name='${googleDriveService.ROOT_FOLDER_NAME}' or name='${name}' and mimeType = 'application/vnd.google-apps.folder' and trashed = false`})
            expect(googleDriveService.drive.files.list).toBeCalledTimes(1);
            expect(folder).toHaveProperty('key', '2')
            expect(folder).toHaveProperty("name", name);
        })
    });

    describe('findFolderByPath', () => {
        /**
         * Happy paths
         * alimentary
         * alimentary/foods
         * rfitness
         * rstock
         * Aclaracion: el path "Storage" lo agrega el servicio internamente por configuracion
         */
        const folders = [
            {
                id: "0",
                name: 'drive'
            },
            {
                id: "1",
                name: googleDriveService.ROOT_FOLDER_NAME,
                parents: ["0"]
            },
            {
                id: "2",
                name: "alimentary",
                parents: ["1"]
            },
            {
                id: "10",
                name: "foods",
                parents: ["2"]
            },
            {
                id: "3",
                name: "rfitness",
                parents: ["1"]
            },
            {
                id: "4",
                name: "rstock",
                parents: ["1"]
            },
        ];

        beforeAll(() => {
            googleDriveService.drive.files.get.mockClear();
            googleDriveService.drive.files.list.mockClear();

            googleDriveService.drive.files.get.mockImplementation((opts: any) => {
                return {
                    data: folders.find(folder => folder.id === opts.fileId)
                }
            })
            googleDriveService.drive.files.list.mockImplementation((opts: any) => {
                const {q} = opts;
                const foundFolders = folders.filter(folder => q.includes(folder.name));
                return {data: {files: foundFolders}};
            });
        });

        it('returns null because folder doesn\'t exists', async () => {
            const result = await googleDriveService.findByPath("not-exists");
            expect(result).toBe(null);
        })

        it('returns root folder', async () => {
            const result = await googleDriveService.findByPath("");
            expect(result.id).toBe("1");
        })

        it('returns expected folder', async () => {
            const result = await googleDriveService.findByPath("alimentary/foods");
            expect(result.id).toBe("10");
        })
    });

    describe.skip('uploadMediaByUrl', () => {
        it('upload a new file in a specify path successfully', () => {

        })
    })
})
