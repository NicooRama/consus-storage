import {
    StorageService,
    ListFilesResponse,
    CreateFolderResponse,
    UploadMediaFileResponse
} from "../../storage.interface";
import axios, {AxiosInstance, AxiosResponse} from "axios";
import {config} from "../../../core/config";
import {ImgBBUploadResponse} from "./imgBB.interface";
import FormData from 'form-data';

export class ImgBBService implements StorageService {
    keyName: string = "IMG_BB";
    instance: AxiosInstance;

    constructor() {
        this.instance = axios.create({
            baseURL: `https://api.imgbb.com/1`,
        })
    }

    createFolder(name: string, pathTo: string | undefined): Promise<CreateFolderResponse> {
        throw new Error('Method not supported');
    }

    listFiles(path: string): Promise<ListFilesResponse> {
        throw new Error('Method not supported');
    }

    uploadMediaByBase64(name: string, pathTo: string, base64: string): Promise<UploadMediaFileResponse> {
        return Promise.resolve(undefined);
    }

    async uploadMediaByUrl(name: string, pathTo: string, url: string): Promise<UploadMediaFileResponse> {
        try {
            const form = new FormData();
            form.append('image', 'https://webkit.org/demos/srcset/image-src.png');
            form.append('name', 'alimentary-kiten');

            const response: AxiosResponse<ImgBBUploadResponse> = await axios.post(
                'https://api.imgbb.com/1/upload',
                form,
                {
                    params: {
                        'key': 'a3acb4c86537a01edd3ee8703e345b26',
                    },
                    headers: {
                        ...(form as any).getHeaders()
                    }
                }
            );
            return {
                name: response.data.data.image.name,
                key: response.data.data.image.name,
                previewLink: response.data.data.display_url,
                link: response.data.data.url,
            }
        } catch (e) {
            console.log(config.imgBBApiKey)
            console.log(e.response.data.error)
            console.error(e);
        }
    }

}
