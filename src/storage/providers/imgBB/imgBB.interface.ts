interface Image {
    filename: string;
    name: string;
    mime: string;
    extension: string;
    url: string;
}

export interface ImgBBUploadResponse {
    data: {
        id: string;
        title: string;
        url_viewer: string;
        url: string;
        display_url: string;
        width: number;
        height: number;
        size: number;
        time: number;
        expiration: number;
        image: Image;
        thumb: Image;
        delete_url: string;
    };
    success: boolean;
    status: number;
}
