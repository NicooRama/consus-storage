import {
    CreateFolderResponse,
    CreateFolderResult,
    ListFilesResponse,
    StorageService,
    UploadMediaFileResponse
} from "../../storage.interface";
import axios, {AxiosResponse} from 'axios';
import FormData from 'form-data';
import {config} from "../../../core/config";
import {ImgUrCreateAlbumResponse, ImgUrUploadImageResponse} from "./imgur.interface";
import {FolderModel} from "../../../db/folder.model";
import {readFileToStream, writeBufferToFile} from "../../../shared/file.utils";
import fs from "fs";
import {Folder} from "../../../db/folder.interface";
import {getImageByUrl} from "../../storage.utils";

export class ImgurService implements StorageService {
    keyName = 'IMG_UR';


    instance = axios.create({
        baseURL: `https://api.imgur.com/3`
    });

    /**
     * Esta api no tiene forma de saber si el album ya existe, hay que verificarlo por base de datos
     * @param name
     * @param pathTo
     */
    async createFolder(name: string, pathTo: string | undefined): Promise<CreateFolderResponse> {
        const folder = await FolderModel.findOne({name});
        if (folder) {
            return {
                name,
                key: folder.externalId,
                result: CreateFolderResult.NOT_MODIFIED
            }
        }

        const form = new FormData();
        form.append('title', name);

        let response: AxiosResponse<ImgUrCreateAlbumResponse>

        response = await this.instance.post(`/album`, form, {
            headers: {
                ...form.getHeaders(),
                Authorization: `Bearer ${config.imgUrAccessToken}`
            }
        });

        await new FolderModel({
            name,
            provider: this.keyName,
            externalId: response.data.data.id
        }).save();

        return {
            name,
            key: response.data.data.id,
            result: CreateFolderResult.CREATED
        }
    }

    listFiles(path: string): Promise<ListFilesResponse> {
        return Promise.resolve(undefined);
    }

    async uploadMedia(name: string, pathTo: string, dataToPersist: string) {
        const folder: Folder = await FolderModel.findOne({name: pathTo});
        if (!folder) {
            throw new Error('Folder not found')
        }

        const buffer = Buffer.from(dataToPersist, "base64")

        try {
            await writeBufferToFile(name, buffer);
        } catch (e) {
            throw e;
        }

        const stream = readFileToStream(name);

        const form = new FormData();
        form.append('image', stream);
        form.append('album', folder.externalId);
        form.append('name', name);

        const response: AxiosResponse<ImgUrUploadImageResponse> = await this.instance.post(
            '/upload',
            form,
            {
                headers: {
                    ...form.getHeaders(),
                    Authorization: `Bearer ${config.imgUrAccessToken}`
                }
            });

        return {
            name: response.data.data.name,
            key: response.data.data.id,
            link: response.data.data.link,
            previewLink: response.data.data.link
        };
    }

    async uploadMediaByBase64(name: string, pathTo: string, base64: string): Promise<UploadMediaFileResponse> {
        return await this.uploadMedia(name, pathTo, base64.split(/,(.+)/)[1])
    }

    async uploadMediaByUrl(name: string, pathTo: string, url: string): Promise<UploadMediaFileResponse> {
        let response;
        try {
            response = await getImageByUrl(url);
        } catch (e) {
            throw e;
        }
        return await this.uploadMedia(name, pathTo, response);
    }

}
