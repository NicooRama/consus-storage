export interface ImgUrCreateAlbumResponse {
    data : {
        id: string,
        deletehash: string,
    },
    success: boolean;
    status: number;
}

export interface ImgUrUploadImageResponse {
    data: {
        id: string;
        deletehash: string;
        account_id: number;
        account_url: string;
        ad_type?: any;
        ad_url?: any;
        title?: any;
        description?: any;
        name: string;
        type: string;
        width: number;
        height: number;
        size: number;
        views: number;
        section?: any;
        vote?: any;
        bandwidth: number;
        animated: boolean;
        favorite: boolean;
        in_gallery: boolean;
        in_most_viral: boolean;
        has_sound: boolean;
        is_ad: boolean;
        nsfw?: any;
        link: string;
        tags: any[];
        datetime: number;
        mp4: string;
        hls: string;
    },
    status: number;
    success: boolean;
}
