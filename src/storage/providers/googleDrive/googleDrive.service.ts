import {
    CreateFolderResponse,
    CreateFolderResult,
    ListFilesResponse,
    StorageService,
    UploadMediaFileResponse
} from "../../storage.interface";
import * as fs from "fs";
import {readFileSync} from "fs";
import {google} from "googleapis";
import {OAuth2Client} from "google-auth-library";
import {normalizeAndSplitInParts, normalizePath, readFileToStream, writeBufferToFile} from "../../../shared/file.utils";
import axios from "axios";
import {config} from '../../../core/config';
import {getImageByUrl} from "../../storage.utils";

enum PersistMode {
    CREATE,
    UPDATE,
}

export class GoogleDriveService implements StorageService {
    keyName = "GOOGLE_DRIVE";

    TOKEN_PATH = config.tokenPath;
    CREDENTIALS_PATH = config.credentialsPath;
    ROOT_FOLDER_NAME = config.rootFolderName;

    oAuth2Client: OAuth2Client;
    drive: any;

    constructor() {
        const credentials = JSON.parse(readFileSync(this.CREDENTIALS_PATH) as unknown as string);
        const {client_secret, client_id, redirect_uris} = credentials.installed;
        this.oAuth2Client = new google.auth.OAuth2(
            client_id, client_secret, redirect_uris[0]);
        const token = JSON.parse(readFileSync(this.TOKEN_PATH) as unknown as string)
        this.oAuth2Client.setCredentials(token);
        this.drive = google.drive({version: 'v3', auth: this.oAuth2Client});
    }

    listFiles = async (path: string): Promise<ListFilesResponse> => {
        const pathToFind = normalizePath(path);
        const folder = await this.findByPath(pathToFind, {onlyFolders: true});
        const files = await this.findFiles(`'${folder.id}' in parents`);
        return files.map((file) => ({
            name: file.name,
            key: file.id,
            link: this.buildLink(file.id),
            previewLink: this.buildPreviewLink(file.id)
        }))
    }

    createFolder = async (name: string, pathTo: string = ""): Promise<CreateFolderResponse> => {
        const pathToStorage = normalizePath(pathTo);
        const pathToFind = `${pathToStorage}/${name}`;
        const folder = await this.findByPath(pathToFind, {onlyFolders: true});

        if (folder) {
            return {
                key: folder.id,
                name: folder.name,
                result: CreateFolderResult.NOT_MODIFIED,
            }
        }

        const parentFolder = await this.findByPath(pathToStorage, {onlyFolders: true});

        let result: any;
        try {
            result = await this.drive.files.create({
                resource: {
                    name,
                    mimeType: 'application/vnd.google-apps.folder',
                    parents: [parentFolder.id]
                },
            });
        } catch (e) {
            throw e;
        }

        const file = result.data;
        return {
            key: file.id,
            name: file.name,
            result: CreateFolderResult.CREATED,
        };
    }

    findFiles = async (query: string, {onlyFolders = false} = {}): Promise<any[]> => {
        try {
            const result = await this.drive.files.list({
                q: `${query}${onlyFolders ? " and mimeType = 'application/vnd.google-apps.folder' " : " "}and trashed = false`,
                fields: 'files(id, name, parents)',
            })
            return result.data.files;
        } catch (e) {
            throw e;
        }
    }

    findByPath = async (path: string = "", {onlyFolders = false} = {}): Promise<any> => {
        const pathParts = normalizeAndSplitInParts(path);
        const queryNames = [this.ROOT_FOLDER_NAME, ...pathParts].map(part => `name='${part}'`).join(' or ')
        // asumo que son folders por ahora
        const folders = await this.findFiles(queryNames, {onlyFolders});

        let parent = folders.find(folder => folder.name === this.ROOT_FOLDER_NAME);

        for (let i = 0; i < pathParts.length; i++) {
            parent = folders.find(folder => folder.name.toLowerCase() === pathParts[i].toLowerCase() && folder.parents.some(folderParent => folderParent === parent.id));
            if (!parent) {
                return null;
            }
        }
        return parent;
    }

    uploadMediaByUrl = async (name: string, pathTo: string, url: string): Promise<UploadMediaFileResponse> => {
        let response;
        try {
            response = await getImageByUrl(url);
        } catch (e) {
            throw e;
        }
        return await this.uploadMedia(name, pathTo, response);
    }

    uploadMediaByBase64 = async (name: string, pathTo: string, base64: string): Promise<UploadMediaFileResponse> => {
        return await this.uploadMedia(name, pathTo, base64.split(/,(.+)/)[1]);
    }

    /**
     * @param name: ei. photo.jpg
     * @param pathTo: ei. foo/bar
     * @param dataToPersist
     */
    uploadMedia = async (name: string, pathTo: string, dataToPersist: any): Promise<UploadMediaFileResponse> => {
        const pathToStorage = normalizePath(pathTo);
        const pathToFind = `${pathToStorage}/${name}`;

        const file = await this.findByPath(pathToFind, {onlyFolders: false});

        const mode = !file ? PersistMode.CREATE : PersistMode.UPDATE;

        const parentFolder = await this.findByPath(pathToStorage, {onlyFolders: true});

        const buffer = Buffer.from(dataToPersist, "base64")

        try {
            await writeBufferToFile(name, buffer);
        } catch (e) {
            throw e;
        }

        const stream = readFileToStream(name);

        const fileMetadata = {
            'name': name,
        };

        const media = {
            mimeType: `image/${name.split('.').pop()}`,
            body: stream
        };

        let result;
        try {
            if (mode === PersistMode.CREATE) {
                result = await this.drive.files.create({
                    resource: {
                        ...fileMetadata,
                        parents: [parentFolder.id],
                    },
                    media: media,
                });
            } else {
                result = await this.drive.files.update({
                    fileId: file.id,
                    resource: fileMetadata,
                    media: media,
                });
            }

        } catch (e) {
            throw e;
        }

        fs.unlink(name, (err) => {
            if (err) {
                console.log(err)
            }
        });

        const {data} = result;
        const {id} = data;


        return {
            name,
            key: id,
            link: this.buildLink(id),
            previewLink: this.buildPreviewLink(id),
        };
    }

    buildLink(id: string) {
        return `https://drive.google.com/file/d/${id}/view`
    }

    buildPreviewLink(id: string) {
        return `https://drive.google.com/uc?export=download&id=${id}`
    }
}
