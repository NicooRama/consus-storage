import {
    CreateFolderResponse,
    CreateFolderResult, ListFilesResponse,
    StorageService,
    UploadMediaFileResponse
} from "../../storage.interface";
import axios, {AxiosInstance, AxiosResponse} from "axios";
import {config} from "../../../core/config";
import {SirvCreateFolderResponse, SirvFile, SirvLoginResponse, SirvReadFolderResponse} from "./sirv.interface";
import {getImageByUrl} from "../../storage.utils";
import {normalizePath, readFile, readFileToStream, writeBufferToFile} from "../../../shared/file.utils";

export class SirvService implements StorageService {
    keyName: string = "SIRV";
    instance: AxiosInstance;
    token: string;
    constructor() {
        this.instance = axios.create({
            baseURL: `https://api.sirv.com/v2`,
        })
    }

    async login(): Promise<string> {
        const response:AxiosResponse<SirvLoginResponse> = await this.instance.post(
            '/token',
            {
                'clientId': config.sirvClientId,
                'clientSecret': config.sirvClientSecret,
            },
        );
        this.token = response.data?.token;
        return this.token;
    }

    async createFolder(name, pathTo: string = ''): Promise<CreateFolderResponse> {
        const exists = await this.existsFolder(name, pathTo);
        if(exists) {
            return {
                name: name,
                key: [pathTo,name].join('/'),
                result: CreateFolderResult.NOT_MODIFIED
            }
        }
        const response: AxiosResponse<SirvCreateFolderResponse> = await this.instance.post(
            '/files/mkdir',
            {
                'dirname': pathTo ? [pathTo,name].join('/') : name,
            },
            {
                headers: {
                    Authorization: `Bearer ${this.token}`
                }
            }
        );
        return {
            name: response.data.name,
            key: [pathTo,name].join('/'),
            result: CreateFolderResult.CREATED
        }
    }

    async readFolder(path: string, token: string): Promise<SirvFile[]> {
        const response: AxiosResponse<SirvReadFolderResponse> = await this.instance.get('/files/readdir', {
            params: {
                dirname: path
            },
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        return response.data.contents;
    }

    async existsFolder(name: string, path: string): Promise<boolean> {
        const files = await this.readFolder(path, this.token);
        return files.some(file => file.isDirectory && file.filename === name);
    }

    async getFile(name: string): Promise<SirvFile> {
        const response: AxiosResponse<SirvFile> = await this.instance.get('/files/stat', {
            params: {
                filename: name
            },
            headers: {
                Authorization: `Bearer ${this.token}`
            }
        });
        return response.data;
    }

    async listFiles(path: string): Promise<ListFilesResponse> {
        throw new Error('Method not implemented.');
    }

    async uploadMediaByBase64(name: string, pathTo: string, base64: string): Promise<UploadMediaFileResponse> {
        return await this.uploadMedia(name, pathTo, base64.split(/,(.+)/)[1]);
    }

    async uploadMediaByUrl(name: string, pathTo: string, url: string): Promise<UploadMediaFileResponse> {
        let response;
        try {
            response = await getImageByUrl(url);
        } catch (e) {
            throw e;
        }
        return await this.uploadMedia(name, pathTo, response);
    }

    uploadMedia = async (name: string, pathTo: string, dataToPersist: any): Promise<UploadMediaFileResponse> => {
        await this.login();
        const pathToFind = ["", config.rootFolderName, ...normalizePath(pathTo).split('/')].join('/');
        const pathToStorage = `${pathToFind}/${name}`;

        const buffer = Buffer.from(dataToPersist, "base64")
        try {
            await writeBufferToFile(name, buffer);
        } catch (e) {
            throw e;
        }
        const data = readFile(name);

        await this.instance.post('/files/upload', data, {
            headers: {
                'content-type': `image/${name.split('.').pop()}`,
                authorization: `Bearer ${this.token}`
            },
            params: {
                filename: pathToStorage
            }
        });

        return {
            name: name,
            key: pathToStorage,
            previewLink: `https://partatyl.sirv.com${pathToStorage}`,
            link: `https://partatyl.sirv.com${pathToStorage}`
        }
    }
}
