export interface SirvLoginResponse {
    token: string;
    expiresIn: number;
    scope: string[];
}

export interface SirvReadFolderResponse {
    contents: SirvFile[];
}

export interface SirvCreateFolderResponse {
	name: string;
	message: string;
}

export interface SirvFile {
	filename: string;
	size: number;
	meta: {
        duration: number;
        width: number;
        height: number;
    };
	mtime: string;
	contentType: string;
	isDirectory: boolean;
}

