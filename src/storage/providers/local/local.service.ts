import {
    CreateFolderResponse,
    CreateFolderResult, ListFilesResponse,
    StorageService,
    UploadMediaFileResponse
} from "../../storage.interface";
import pathLib from 'path';

import fsCbs from 'fs';
import {StrUtils} from "@consus/js-utils";
import {config} from "../../../core/config";
import {getImageByUrl, getExtensionFromArrayBuffer} from "../../storage.utils";

const fs = fsCbs.promises;

export class LocalStorageService implements StorageService {
    keyName = 'LOCAL_STORAGE';
    rootPath = config.rootPath;
    host = config.host;

    constructor() {
    }

    private generateKey = (path: string) => {
        return StrUtils.removeAccents(path)
            .replace(/\./g, '-').replace(/\//g, '-').replace(/\\/g, '-').trim().replace(/ /g, '-').toLowerCase();
    }

    private getAbsolutePath = (path: string, name?: string) => {
        return pathLib.join(this.rootPath, path, name || '');
    }

    createFolder = async (name: string, pathTo?: string): Promise<CreateFolderResponse> => {
        const relativePath = pathLib.join(pathTo || '', name);
        const absolutePath = this.getAbsolutePath(relativePath);
        const key = this.generateKey(relativePath);
        const resultPath = await fs.mkdir(absolutePath, {recursive: true});
        if(!resultPath) {
            return {
                name,
                key,
                result: CreateFolderResult.NOT_MODIFIED
            }
        }

        return {
            name,
            key,
            result: CreateFolderResult.CREATED
        }
    }

    generateLink = (path: string) => {
        return `${this.host}/${path}`;
    }

    uploadMediaByUrl = async (name: string, pathTo: string, url: string): Promise<UploadMediaFileResponse> => {
        const response = await getImageByUrl(url);
        const buffer = Buffer.from(response);

        if(!name.includes('.')) {
            name = name + '.' + getExtensionFromArrayBuffer(buffer);
        }

        const relativePath = pathLib.join(pathTo, name);
        const absolutePath = this.getAbsolutePath(relativePath);
        const key = this.generateKey(relativePath);

        await fs.appendFile(absolutePath, buffer);
        const link = this.generateLink(relativePath);
        return {
            name,
            key,
            previewLink: link,
            link: link
        }
    }

    uploadMediaByBase64 = async (name: string, pathTo: string, base64: string): Promise<UploadMediaFileResponse> => {
        const relativePath = pathLib.join(pathTo, name);
        const absolutePath = this.getAbsolutePath(relativePath);
        const key = this.generateKey(relativePath);
        const matches = base64.match(/^data:([A-Za-z-+/]+);base64,(.+)$/);
        // const type = matches[1];
        const data = matches[2];
        await fs.appendFile(absolutePath, Buffer.from(data, 'base64'));
        const link = this.generateLink(relativePath);
        return {
            name,
            key,
            previewLink: link,
            link: link
        }
    }

    listFiles = async (path: string): Promise<ListFilesResponse> => {
        const filesAndFolders = await fs.readdir(this.getAbsolutePath(path));
        const files = filesAndFolders.filter(file => fsCbs.statSync(this.getAbsolutePath(path, file)).isFile());
        return files.map(file => ({
            name: file,
            key: this.generateKey(pathLib.join(path, file)),
            previewLink: this.generateLink(pathLib.join(path, file)),
        }));
    }
}
