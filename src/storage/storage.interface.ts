export interface StorageService {
    keyName: string;

    createFolder: (name: string, pathTo?: string) => Promise<CreateFolderResponse>
    uploadMediaByUrl: (name: string, pathTo: string, url: string) => Promise<UploadMediaFileResponse>
    uploadMediaByBase64: (name: string, pathTo: string, base64: string) => Promise<UploadMediaFileResponse>
    listFiles: (path: string) => Promise<ListFilesResponse>;
}

export enum CreateFolderResult {
    CREATED = 'CREATED',
    NOT_MODIFIED = 'NOT_MODIFIED',
}

export type CreateFolderResponse = {
    name: string,
    key: string,
    result: CreateFolderResult
};

export type ListFilesResponse = {
    name: string;
    key: string;
    previewLink: string;
}[]

export type UploadMediaFileResponse = {
    name: string,
    key: string,
    previewLink?: string,
    link: string
}
