# MVP
### Creacion de carpetas
- Endpoint para poder crear una carpeta que va a usar el proyecto para el almacenamiento.
  - Adentro de esa carpeta el proyecto va a poder crear subcarpetas segun le sea conveniente.

### Subir un archivo
- Subir un archivo a la carpeta deseada y crear un enlace publico a dicho archivo.
  - La respuesta debera estar conformada por:
    - Link publico
    - Link a la preview del archivo (ideal para imagenes y gifs), ejemplo: https://drive.google.com/uc?export=download&id=:id
      1g6X-g8fuZSofAWYkmU33kgq-o8cOcw2h
### Listado de archivos y carpetas
- Endpoint para que los proyectos puedan listar sus carpetas y archivos.


# v2.0
### Seguridad
- Implementar una capa de seguridad para que los proyectos no puedan
  - Almacenar archivos en carpetas de otros proyectos. 
  - Leer archivos que no sean suyos (es decir que no esten en el root de ese proyecto).
  - Listar archivos y carpetas que no sean suyas.
